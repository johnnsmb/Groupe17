#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <thread>
#include <unistd.h>
#include <cstdlib>
#include "blanc.h"
#include "IA.h"
using std::ifstream;
using std::ofstream;
using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::getline;

Blanc::Blanc()
{
	}
	
bool Blanc::getJeuIA(){
	return jeuIA;
}

void Blanc::setJeuIA(bool jeuInt){
	jeuIA = jeuInt;
}

void Blanc::go_b(bool jeuIntelligent)
{	
	setJeuIA(jeuIntelligent);
	string nom_fichier_blanc("./blanc.txt");
    ofstream fichier_blanc(nom_fichier_blanc);
    ifstream fichier_blanc_lecture(nom_fichier_blanc);
	ifstream fichier_noir("./noir.txt");
	
	while (!fichier_noir.is_open())
	{
		cout << "Attente du joueur noir (fichier noir indisponible)" << endl;//Attente avec chrono
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		fichier_noir.open("./noir.txt");
	}
	
	Tableau tableau;
	string replique="";
	while (replique != "[THE END]"  && replique != "stop")
	{
		int k=0;														//
		cout<< "C'est le tour du joueur noir."<<endl;					//
		do { 															//
			if(getline(fichier_noir, replique)){						//
				Tableau tabVer=tableau;									//
				tabVer.recherchePlacesLibres("noir");					//
				k=tabVer.choisirCase2("noir",replique);					//
			}															//
			fichier_noir.clear();										//attente plus verification entree de l'ennemi
			fichier_noir.sync();										//
		}																//
		while (k == 0);													//
		if(k==2){														//
			cout<< "Joueur noir quitte la partie."<<endl;				//
			break;														//
		}																//
		if(k==5)
			break;
		tableau.recherchePlacesLibres("noir");							//update tableau
		tableau.choisirCase2("noir",replique);							//
		cout << "Joueur noir a choisit la case : " << replique << endl;	//
	
		
		do { 															//attente verification de l'entree du joueur
			Tableau tabVer=tableau;										//
			if (getJeuIA()){											//										
				IA ia;													//
				string res=ia.AlphaBetaSearch(tabVer,"blanc");			//		
				fichier_blanc<<res<<endl;								//
				k=tabVer.choisirCase2("blanc",replique);				//
			}															//		
			else														//										
				cout << "Joueur blanc, c'est ton tour : " << endl;		//
																		//
			tabVer.recherchePlacesLibres("blanc");						//
			tabVer.imprimeTableau();									//
			while (!(getline(fichier_blanc_lecture, replique))){		//
				fichier_blanc_lecture.clear();							//
				fichier_blanc_lecture.sync();							//
			}															//
			k=tabVer.choisirCase2("blanc",replique);					//
			tabVer.cleanHashtag();										//
			tabVer.imprimeTableau();									//
		}																//
		while (k == 0);													//
		if(k==2){														//
			cout<< "Tu as quitte la partie."<<endl;						//
			break;														//
		}																//
		if(k==5)
			break;
		tableau.recherchePlacesLibres("blanc");							//update tableau							
		tableau.choisirCase2("blanc",replique);							//
		
		
	
	
}}
